<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Site_Controller extends MX_controller {
	
	var $_container;
	var $data;

	public function __construct()
	{
		parent::__construct();

		$this->load->add_package_path(FCPATH.'themes/'. config_item('theme'));
		
		$this->load->helper(array('text','theme'));

		$this->_container = "container.php";
	}

}