<?php
	class Branches extends MX_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('branch_model');
	}

	public function index(){
		$data['branch'] = $this->branch_model->all();
		$this->load->view('branch/index', $data);
	}

	public function create(){
		
		if($_POST){
			$this->form_validation->set_rules('name','Name','required');
			$this->form_validation->set_rules('address','Address','required');
			$this->form_validation->set_rules('telephone','Telephone','required');
			$this->form_validation->set_rules('email','Email','required');
			$this->form_validation->set_rules('fax','Fax','required');
			$this->form_validation->set_rules('manager','Manager','required');

			if($this->form_validation->run()==TRUE){
				$formArray = array();
				$formArray['name']	= $this->input->post('name');		
				$formArray['address']	= $this->input->post('address');		
				$formArray['telephone']	= $this->input->post('telephone');		
				$formArray['email']	= $this->input->post('email');		
				$formArray['fax']	= $this->input->post('fax');		
				$formArray['manager']	= $this->input->post('manager');

				$this->branch_model->create($formArray);						

			}
		}
		$this->load->view('create');
	}
	public function update(){

		if($_POST){
			$this->form_validation->set_rules('name','Name','required');
			$this->form_validation->set_rules('address','Address','required');
			$this->form_validation->set_rules('telephone','Telephone','required');
			$this->form_validation->set_rules('email','Email','required');
			$this->form_validation->set_rules('fax','Fax','required');
			$this->form_validation->set_rules('manager','Manager','required');

			if($this->form_validation->run()==TRUE){
				$formArray = array();
				$formArray['name']	= $this->input->post('name');		
				$formArray['address']	= $this->input->post('address');		
				$formArray['telephone']	= $this->input->post('telemanagerphone');		
				$formArray['email']	= $this->input->post('email');		
				$formArray['fax']	= $this->input->post('fax');		
				$formArray['manager']	= $this->input->post('manager');

				$this->branch_model->updateBranch($formArray);						

			}
		}
		$this->load->view('branch/index');
		
	}
	public function delete(){
		$this->branch_model->getBranchByID($branchId);
		$this->branch_model->deleteBranch($branchId);
		redirect('branch');

		
	}
}