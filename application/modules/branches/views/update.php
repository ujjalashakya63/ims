<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Branch<small>Create</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Branch</a></li>
      </ol>
    </section>

    <section class="content">
      <!-- Default box -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Create Branch</h3>
        </div>
        <div class="box-body">

			<form method="POST" name="updatebranch" action="<?php echo base_url(). 'branches/update/'.$branch['id'];?>">
				<div class="row">
					<div class="col-md-12">

						<div class="form-group">
							<label>Name</label>
							<input type="text" name="name" value="<?php echo set_value('name', $branch['name']);?>" class="form-control">
							<?php echo form_error('name');?>
						</div>

						<div class="form-group">
							<label>Address</label>
							<input type="text" name="address" value="<?php echo set_value('address',$branch['address']);?>" class="form-control">
							<?php echo form_error('address');?>
						</div>
						

						<div class="form-group">
							<label>Telephone</label>
							<input type="text" name="telephone"  value="<?php echo set_value('telephone', $branch['telephone']);?>"  class="form-control">
							<?php echo form_error('telephone');?>
						</div>

						<div class="form-group">
							<label>Email</label>
							<input type="text" name="email"  value="<?php echo set_value('email', $branch['email']);?>"  class="form-control">
							<?php echo form_error('email');?>
						</div>

						<div class="form-group">
							<label>Fax</label>
							<input type="text" name="fax"  value="<?php echo set_value('fax', $branch['fax']);?>"  class="form-control">
							<?php echo form_error('fax');?>
						</div>

						<div class="form-group">
							<label>Manager</label>
							<input type="text" name="manager"  value="<?php echo set_value('manager', $branch['manager']);?>"  class="form-control">
							<?php echo form_error('manager');?>
						</div>
					
						<div class="form-group">
							<button type='submit' class="btn btn-primary">Create</button>
							<a href="<?php echo base_url('branch/create');?>" class="btn-secondary btn">Cancel</a>
						</div>						
					</div>
				</div>
			</form> 
	    </div>
	  </div>

    </section>
    <!-- /.content -->
  </div>
</div>