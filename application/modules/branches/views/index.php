<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Branch
        <small>List</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Branches</li>
      </ol>
    </section>
    <section class="content">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Branch list</h3>
        </div>
        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
                <thead>
	                <tr>
						<th>S.NO.</th>
						<th>Name</th>
						<th>Address</th>
						<th>Telephone</th>
						<th>Email</th>
						<th>Fax</th>
						<th>manager</th>
						<th>Action</th>
					</tr>
                </thead>
                <tbody>
	            	<?php //echo "<pre>"; print_r($products); die();
						if($branch) { 
							$n = 1;
							for($i = 0; $i < count($branch); $i++) {?>
		                <tr>
		                  <td><?php echo $n; ?></td>
							<td><?php echo $branch[$i]['name'];?> </td>
							<td><?php echo $branch[$i]['address'];?> </td>
							<td><?php echo $branch[$i]['telephone'];?> </td>
							<td><?php echo $branch[$i]['email'];?> </td>
							<td><?php echo $branch[$i]['fax'];?> </td>
							<td><?php echo $branch[$i]['manager'];?> </td>
							<td>
								<a class="btn btn-success" href="<?php echo site_url('branches/updtae_branch/'.$branch[$i]['id'])?>" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
	                            <a class="btn btn-danger" href="<?php echo site_url('branches/delete/'.$products[$i]['id'])?>" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
			 				</td>
		                </tr>
		            <?php $n++; }} ?>    
                </tbody>
                
          </table>
        </div>
      </div>

    </section>
  </div>
