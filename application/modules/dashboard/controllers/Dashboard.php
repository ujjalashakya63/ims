<?php 

class Dashboard extends Site_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index() {

		$this->data['header'] = "Dashboard";
		$this->data['view_page'] = 'dashboard/index';
			
		$this->load->view($this->_container, $this->data);

	}

	// public function create() {
	// 	$this->data['header'] = "Create";
	// 	$this->data['view_page'] = 'dashboard/create';
			
	// 	$this->load->view($this->_container, $this->data);
	// }

}		

		